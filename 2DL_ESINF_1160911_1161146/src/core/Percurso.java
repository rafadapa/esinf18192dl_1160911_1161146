/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import graph.Graph;
import java.util.LinkedList;
import java.util.Objects;

/**
 *
 * @author 1160911 <Rafael Teixeira>
 */
public class Percurso {
    
   private LinkedList<Estacao> estacoesPercorridas;
   private LinkedList<Double> tempos;
   private Double instanteInicial;

    public Percurso() {
    }
   
   
   
   public Percurso(LinkedList<Estacao> estacoesPercorridas, Double instanteInicial){
        this.estacoesPercorridas = estacoesPercorridas;
        tempos = new LinkedList<>();
        this.instanteInicial = instanteInicial;
    }

    public Percurso(LinkedList<Estacao> estacoesPercorridas, LinkedList<Double> tempos, Double instanteInicial) {
        this.estacoesPercorridas = estacoesPercorridas;
        this.tempos = tempos;
        this.instanteInicial = instanteInicial;
    }
   
   

    public LinkedList<Estacao> getEstacoesPercorridas() {
        return estacoesPercorridas;
    }

    public void setEstacoesPercorridas(LinkedList<Estacao> estacoesPercorridas) {
        this.estacoesPercorridas = estacoesPercorridas;
    }

    public LinkedList<Double> getTempos() {
        return tempos;
    }

    public void setTempos(LinkedList<Double> tempos) {
        this.tempos = tempos;
    }

    public Double getInstanteInicial() {
        return instanteInicial;
    }

    public void setInstanteInicial(Double instanteInicial) {
        this.instanteInicial = instanteInicial;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.estacoesPercorridas);
        hash = 23 * hash + Objects.hashCode(this.tempos);
        hash = 23 * hash + Objects.hashCode(this.instanteInicial);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Percurso other = (Percurso) obj;
        if (!Objects.equals(this.estacoesPercorridas, other.estacoesPercorridas)) {
            return false;
        }
        if (!Objects.equals(this.tempos, other.tempos)) {
            return false;
        }
        if (!Objects.equals(this.instanteInicial, other.instanteInicial)) {
            return false;
        }
        return true;
    }
   
    
    
    
    public void preencherListaTempos(Graph g){
        tempos.add(instanteInicial);
        Double tempo = instanteInicial;
        for (int i = 0; i< estacoesPercorridas.size(); i++) {
            if (i + 1 == estacoesPercorridas.size()) {
                break;
            }
            tempo += g.getEdge(estacoesPercorridas.get(i), estacoesPercorridas.get(i + 1)).getWeight();
            tempos.add(tempo);
        }
    }
    
    
    
    
    
    
}
