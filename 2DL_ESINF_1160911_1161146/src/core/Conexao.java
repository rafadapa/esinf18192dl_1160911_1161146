/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.Objects;

/**
 *
 * @author Rafael Teixeira
 */
public class Conexao {
    
    String linha;
    Estacao e1;
    Estacao e2;
    int tempoMedio;

    public Conexao(String linha, Estacao e1, Estacao e2, int tempoMedio) {
        this.linha = linha;
        this.e1 = e1;
        this.e2 = e2;
        this.tempoMedio = tempoMedio;
    }
    
    

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public Estacao getE1() {
        return e1;
    }

    public void setE1(Estacao e1) {
        this.e1 = e1;
    }

    public Estacao getE2() {
        return e2;
    }

    public void setE2(Estacao e2) {
        this.e2 = e2;
    }

    public int getTempoMedio() {
        return tempoMedio;
    }

    public void setTempoMedio(int tempoMedio) {
        this.tempoMedio = tempoMedio;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.linha);
        hash = 47 * hash + Objects.hashCode(this.e1);
        hash = 47 * hash + Objects.hashCode(this.e2);
        hash = 47 * hash + this.tempoMedio;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conexao other = (Conexao) obj;
        if (this.tempoMedio != other.tempoMedio) {
            return false;
        }
        if (!Objects.equals(this.linha, other.linha)) {
            return false;
        }
        if (!Objects.equals(this.e1, other.e1)) {
            return false;
        }
        if (!Objects.equals(this.e2, other.e2)) {
            return false;
        }
        return true;
    }
    
    
    
}
