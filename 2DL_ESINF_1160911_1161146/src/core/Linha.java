/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Toshiba
 */
public class Linha {
    
   private String cod_linha;
   private ArrayList<Estacao> linha = new ArrayList<>();


    public Linha(String cod_linha,ArrayList<Estacao> list) {
        this.cod_linha = cod_linha;
        this.linha = list;
    }

    public Linha() {
    }

    public ArrayList<Estacao> getLinha() {
        return linha;
    }

    public void setLinha(ArrayList<Estacao> linha) {
        this.linha = linha;
    }

    public String getCod_linha() {
        return cod_linha;
    }

    public void setCod_linha(String cod_linha) {
        this.cod_linha = cod_linha;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.cod_linha);
        hash = 17 * hash + Objects.hashCode(this.linha);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Linha other = (Linha) obj;
        if (!Objects.equals(this.cod_linha, other.cod_linha)) {
            return false;
        }
        if (!Objects.equals(this.linha, other.linha)) {
            return false;
        }
        return true;
    }
    

    
    
   
   
    
}
