package AdjencyMap;

import AdjencyMap.AdjacencyMatrixGraph;
import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF
 */
public class EdgeAsDoubleGraphAlgorithms
{

    /**
     * Determine the shortest path to all vertices from a vertex using
     * Dijkstra's algorithm To be called by public short method
     *
     * @param graph Graph object
     * @param sourceIdx Source vertex
     * @param knownVertices previously discovered vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param minDist minimum distances in the path
     *
     */
    private static <V> void shortestPath(AdjacencyMatrixGraph<V, Double> graph, int sourceIdx, boolean[] knownVertices, int[] verticesIndex, double[] minDist)
    {
        LinkedList<V> aux = new LinkedList<V>();

        for (int i = 0; i < graph.numVertices; i++)
        {
            minDist[i] = 99;
        }
        minDist[sourceIdx] = 0;
        aux.add(graph.vertices.get(sourceIdx));

        while (!aux.isEmpty())
        {
            V vOrig = aux.pollLast();
            knownVertices[graph.toIndex(vOrig)] = true;
            for (V Adj : graph.directConnections(vOrig))
            {
                if (minDist[graph.toIndex(Adj)] == 99)
                {
                    minDist[graph.toIndex(Adj)] = minDist[graph.toIndex(vOrig)] + 1;
                    verticesIndex[graph.toIndex(Adj)] = graph.toIndex(vOrig);
                    aux.add(Adj);

                }
            }
        }
    }

    /**
     * Determine the shortest path between two vertices using Dijkstra's
     * algorithm
     *
     * @param graph Graph object
     * @param source Source vertex
     * @param dest Destination vertices
     * @param path Returns the vertices in the path (empty if no path)
     * @return minimum distance, -1 if vertices not in graph or no path
     *
     */
    public static <V> double shortestPath(AdjacencyMatrixGraph<V, Integer> graph, V source, V dest, LinkedList<V> path)
    {
        if (source.equals(dest))
        {
            path.add(source);
            return 0;
        }
        if (!graph.checkVertex(source) || !graph.checkVertex(dest))
        {
            return -1;
        }
        if (path.isEmpty())
        {
            return -1;
        }

        boolean[] knownVertices = new boolean[graph.numVertices];
        int[] verticesIndex;
        for (int i = 0; i < graph.numVertices; i++)
        {
            graph.checkVertex(dest);
        }
        double[] minDist = new double[graph.numVertices];

        int sourceIdx = graph.toIndex(source);

        return path.size();
    }

    /**
     * Recreates the minimum path between two vertex, from the result of
     * Dikstra's algorithm
     *
     * @param graph Graph object
     * @param sourceIdx Source vertex
     * @param destIdx Destination vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param Queue Vertices in the path (empty if no path)
     */
    private static <V> void recreatePath(AdjacencyMatrixGraph<V, Double> graph, int sourceIdx, int destIdx, int[] verticesIndex, LinkedList<V> path)
    {

        path.add(graph.vertices.get(destIdx));
        if (sourceIdx != destIdx)
        {
            destIdx = verticesIndex[destIdx];
            recreatePath(graph, sourceIdx, destIdx, verticesIndex, path);
        }
    }

    /**
     * Creates new graph with minimum distances between all pairs uses the
     * Floyd-Warshall algorithm
     *
     * @param graph Graph object
     * @return the new graph
     */
    public static <V> AdjacencyMatrixGraph<V, Double> minDistGraph(AdjacencyMatrixGraph<V, Double> graph)
    {
        AdjacencyMatrixGraph<V, Double> newGraph = (AdjacencyMatrixGraph<V, Double>) graph.clone();

        for (int k = 0; k < newGraph.numVertices; k++)
        {
            for (int i = 0; i < newGraph.numVertices; i++)
            {
                if (i != k && newGraph.privateGet(i, k) != null)
                {
                    for (int j = 0; j < newGraph.numVertices; j++)
                    {
                        if (i != j && j != k && newGraph.privateGet(k, j) != null)
                        {
                            if (newGraph.privateGet(i, j) == null)
                            {
                                newGraph.insertEdge(i, j, newGraph.privateGet(i, k) + newGraph.privateGet(k, j));
                            } else if (newGraph.privateGet(i, j) > newGraph.privateGet(i, k) + newGraph.privateGet(k, j))
                            {
                                newGraph.privateSet(i, j, newGraph.privateGet(i, k) + newGraph.privateGet(k, j));
                            }
                        }
                    }
                }
            }
        }
        return newGraph;
    }

}