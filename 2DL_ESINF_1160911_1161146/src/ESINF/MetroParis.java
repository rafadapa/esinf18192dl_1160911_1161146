/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ESINF;

import AdjencyMap.AdjacencyMatrixGraph;
import core.Conexao;
import core.Estacao;
import core.Linha;
import core.Percurso;
import graph.Edge;
import graph.Graph;
import graph.Vertex;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author Toshiba
 */
public class MetroParis {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {

        ParisMetro p = new ParisMetro();

        p.lerFicheiros("coordinates.csv");

        Graph<Estacao, Conexao> graph = p.guardarInfoEstacoes("coordinates.csv");

        System.out.println("---------------------------------Estações--------------------------------------");

        Iterator<Estacao> iterator = graph.vertices().iterator();

        System.out.println("");

        while (iterator.hasNext()) {

            Estacao vertice = iterator.next();

            System.out.println(vertice.getNome() + " , " + vertice.getLatitude() + " , " + vertice.getLongitude());

        }
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("---------------------------------Conexões--------------------------------------");

        System.out.println("");
        graph = p.guardarInfoConexao("connections.csv");

        Iterator<Edge<Estacao, Conexao>> it = graph.edges().iterator();

        while (it.hasNext()) {

            Edge<Estacao, Conexao> con = it.next();

            System.out.println(con.getElement().getLinha() + "," + con.getElement().getE1().getNome() + "," + con.getElement().getE2().getNome() + "," + con.getElement().getTempoMedio());

        }

        System.out.println("\n");
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("---------------------------------Conexo--------------------------------------");

        LinkedList<LinkedList<Estacao>> conexo = new LinkedList<>();

        conexo = p.conexo();
        System.out.println("\n");
        System.out.println("O número de linked lists é de " + conexo.size() + " o que faz sentido visto que numa rede de metro um utilizador é sempre capaz de chegar a todas as estações pertencentes à rede!");

//        for (LinkedList<Estacao> ll : conexo) {
//            for (Estacao e : ll) {
//                System.out.println(e.getNome());
//            }
//        }
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("---------------------------------Linhas--------------------------------------");

        ArrayList<Linha> linha = new ArrayList<>();

        linha = p.guardarInfoLinha("lines_and_stations.csv");

        ArrayList<Estacao> est = new ArrayList<>();

        System.out.println("\n");
        System.out.println("A rede de metros de Paris tem " + linha.size() + " linhas diferentes!");

        for (int i = 0; i < linha.size(); i++) {
            System.out.println("\n");
            System.out.println(linha.get(i).getCod_linha());
            System.out.println("\n");
            for (int j = 0; j < linha.get(i).getLinha().size(); j++) {
                System.out.println(linha.get(i).getLinha().get(j).getNome());
            }
        }

        System.out.println("\n");
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("---------------------------------Shortest Path Mínimo Estações--------------------------------------");

        Percurso urso = p.createPercursoMinEstacoes(p.estacaoDesejada("Charonne"), p.estacaoDesejada("Duroc"), 0.0);

        for (int i = 0; i < urso.getEstacoesPercorridas().size(); i++) {

            System.out.println(urso.getEstacoesPercorridas().get(i).getNome());
            System.out.println(urso.getTempos().get(i));

        }

        System.out.println("\n");
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("---------------------------------Shortest Path Mínimo Tempo--------------------------------------");

        Percurso urso2 = p.createPercursoMinTempo(p.estacaoDesejada("Charonne"), p.estacaoDesejada("Duroc"), 0.0);

        for (int i = 0; i < urso2.getEstacoesPercorridas().size(); i++) {

            System.out.println(urso2.getEstacoesPercorridas().get(i).getNome());
            System.out.println(urso2.getTempos().get(i));

        }

    }
}
