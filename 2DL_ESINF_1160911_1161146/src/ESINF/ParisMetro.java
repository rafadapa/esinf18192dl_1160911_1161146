/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ESINF;

import AdjencyMap.AdjacencyMatrixGraph;
import core.Conexao;
import core.Estacao;
import core.Linha;
import core.Percurso;
import graph.Edge;
import graph.Graph;
import graph.GraphAlgorithms;
import graph.Vertex;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author Toshiba
 */
public final class ParisMetro {

    public ParisMetro() throws FileNotFoundException {
        distanceMap = guardarInfoEstacoes("coordinates.csv");
        distanceMap = guardarInfoConexao("connections.csv");
        arrayLinha = guardarInfoLinha("lines_and_stations.csv");
        
    }

    //ALINEA 1
    Graph<Estacao, Conexao> distanceMap = new Graph<>(true);

    public ArrayList<String> lerFicheiros(String nomeF) throws FileNotFoundException {

        ArrayList<String> ficheiroLido = new ArrayList<>();

        Scanner fich = new Scanner(new File(nomeF));

        while (fich.hasNext()) {

            ficheiroLido.add(fich.nextLine());

        }

        return ficheiroLido;

    }

    public Graph<Estacao, Conexao> guardarInfoEstacoes(String nomeFicheiro) throws FileNotFoundException {

        ArrayList<String> lista = this.lerFicheiros(nomeFicheiro);

        for (String l : lista) {
            String[] array = l.split(";");
            Estacao e = new Estacao(array[0], Double.parseDouble(array[1]), Double.parseDouble(array[2]));
            distanceMap.insertVertex(e);
        }

        return distanceMap;
    }

    public Graph<Estacao, Conexao> guardarInfoConexao(String nomeFicheiro) throws FileNotFoundException {
        ArrayList<Conexao> con = new ArrayList<>();
        ArrayList<String> lista = this.lerFicheiros(nomeFicheiro);
        for (String l : lista) {
            String[] array = l.split(";");
            Conexao c = new Conexao(array[0], estacaoDesejada(array[1]), estacaoDesejada(array[2]), Integer.parseInt(array[3]));
            distanceMap.insertEdge(estacaoDesejada(array[1]), estacaoDesejada(array[2]), c, Integer.parseInt(array[3]));
        }

        return distanceMap;

    }

    ArrayList<Linha> arrayLinha = new ArrayList<>();

    public ArrayList<Linha> guardarInfoLinha(String nomeFicheiro) throws FileNotFoundException {
        ArrayList<String> lista = this.lerFicheiros(nomeFicheiro);
        int i;
        int j;
        for (String l : lista) {
            String[] array = l.split(";");
            Linha linhaNova = new Linha(array[0], new ArrayList<Estacao>());
            linhaNova.getLinha().add(estacaoDesejada(array[1]));
            arrayLinha.add(linhaNova);
        }
        for (i = 0; i < arrayLinha.size(); i++) {
            for (j = i + 1; j < arrayLinha.size(); j++) {
                if (arrayLinha.get(i).getCod_linha().equals(arrayLinha.get(j).getCod_linha())) {
                    arrayLinha.get(i).getLinha().add(arrayLinha.get(j).getLinha().get(0));
                    arrayLinha.remove(j);
                    j--;

                }

            }

        }

        return arrayLinha;

    }

    public Estacao estacaoDesejada(String nome) {

        Iterator<Estacao> iterador = distanceMap.vertices().iterator();

        Estacao estacaoDesejada = null;

        while (iterador.hasNext()) {

            Estacao vertice = iterador.next();
            if (vertice.getNome().equals(nome)) {
                estacaoDesejada = vertice;
            }

        }

        return estacaoDesejada;

    }

    //ALINEA 2
    public LinkedList<LinkedList<Estacao>> conexo() {
        LinkedList<LinkedList<Estacao>> componentes = new LinkedList<>();

        Iterable<Estacao> itr = distanceMap.vertices();
        Iterator iterator = itr.iterator();
        boolean[] visited = new boolean[distanceMap.numVertices()];

        while (iterator.hasNext()) {
            Estacao e = (Estacao) iterator.next();
            int eIndex = distanceMap.getKey(e);

            if (!visited[eIndex]) {

                LinkedList<Estacao> listAuxiliar = GraphAlgorithms.DepthFirstSearch(distanceMap, e);
                componentes.addLast(listAuxiliar);

                for (Estacao est : listAuxiliar) {
                    int idx = distanceMap.getKey(est);
                    visited[idx] = true;
                }

            }

        }

        return componentes;
    }

//    ALINEA 4
    public Percurso createPercursoMinEstacoes(Estacao partida, Estacao chegada, Double instanteInicial) {

        if (!distanceMap.validVertex(chegada) || !distanceMap.validVertex(partida)) {
            return null;
        }

        LinkedList<Estacao> estacoes = new LinkedList<>();
        estacoes.add(chegada);
        estacoes.add(partida);
        shortestPath(partida, chegada, estacoes);

        Percurso percurso = new Percurso(estacoes, instanteInicial);
        percurso.preencherListaTempos(distanceMap);
        return percurso;
    }

    private double shortestPath(Estacao vOrig, Estacao vDest, LinkedList<Estacao> shortPath) {
        if (!distanceMap.validVertex(vDest) || !distanceMap.validVertex(vOrig)) {
            return 0;
        }
        shortPath.clear();
        int nverts = distanceMap.numVertices();
        boolean[] visited = new boolean[nverts];
        int[] pathKeys = new int[nverts];
        double[] dist = new double[nverts];
        Estacao[] vertices = distanceMap.allkeyVerts();
        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }

        shortestPathLength(vOrig, vertices, visited, pathKeys, dist);
        getPath(distanceMap, vOrig, vDest, vertices, pathKeys, shortPath);

        if (shortPath.isEmpty()) {
            return 0;
        }

        return dist[distanceMap.getKey(vDest)];
    }

    private static <V, E> void getPath(Graph<V, E> g, V vInicio, V vFim, V[] vertices, int[] pathKeys, LinkedList<V> path) {

        int vDestKey = g.getKey(vFim);
        path.addFirst(vFim);
        while (vDestKey != g.getKey(vInicio)) {
            if (pathKeys[vDestKey] == -1) {
                path.clear();
                break;
            }
            vFim = vertices[pathKeys[g.getKey(vFim)]];
            path.addFirst(vFim);
            vDestKey = g.getKey(vFim);
        }
    }

    private void shortestPathLength(Estacao vOrig, Estacao[] vertices, boolean[] visited, int[] pathKeys, double[] dist) {

        int vOrigKey = distanceMap.getKey(vOrig);

        dist[vOrigKey] = 0;
        while (vOrigKey != -1) {
            vOrig = vertices[vOrigKey];
            visited[vOrigKey] = true;
            for (Estacao vAdj : distanceMap.adjVertices(vOrig)) {
                int vAdjKey = distanceMap.getKey(vAdj);
                if (!visited[vAdjKey] && dist[vAdjKey] > dist[vOrigKey] + 1) {
                    dist[vAdjKey] = dist[vOrigKey] + 1;
                    pathKeys[vAdjKey] = vOrigKey;
                }
            }
            vOrigKey = getVertMinDist(dist, visited);
        }
    }

    private static int getVertMinDist(double[] distance, boolean[] visited) {

        int verticeKey = -1;
        double minDistance = Double.POSITIVE_INFINITY;

        for (int i = 0; i < distance.length; i++) {
            if (distance[i] < minDistance && !visited[i]) {
                minDistance = distance[i];
                verticeKey = i;
            }
        }
        return verticeKey;
    }

    //ALINEA 5
    public Percurso createPercursoMinTempo(Estacao partida, Estacao chegada, Double instanteInicial) {

        if (!distanceMap.validVertex(chegada) || !distanceMap.validVertex(partida)) {
            return null;
        }

        LinkedList<Estacao> estacoes = new LinkedList<>();
        GraphAlgorithms.shortestPath(distanceMap, partida, chegada, estacoes);
        Percurso percurso = new Percurso(estacoes, instanteInicial);
        percurso.preencherListaTempos(distanceMap);
        return percurso;
    }

}
