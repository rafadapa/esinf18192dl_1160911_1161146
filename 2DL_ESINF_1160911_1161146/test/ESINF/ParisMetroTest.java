/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ESINF;

import core.Conexao;
import core.Estacao;
import core.Linha;
import core.Percurso;
import graph.Graph;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Toshiba
 */
public class ParisMetroTest {

    public ParisMetroTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of lerFicheiros method, of class ParisMetro.
     */
    @Test
    public void testLerFicheiros() throws Exception {
        System.out.println("lerFicheiros");
        String nomeF = "testLs.csv";
        ParisMetro instance = new ParisMetro();
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("1;La Defense");
        expResult.add("1;Esplanade de La Defense");
        expResult.add("1;Pont de Neuilly");
        expResult.add("1;Les Sablons");
        ArrayList<String> result = instance.lerFicheiros(nomeF);
        assertEquals(expResult, result);
    }

    /**
     * Test of guardarInfoEstacoes method, of class ParisMetro.
     */
    @Test
    public void testGuardarInfoEstacoes() throws Exception {
        System.out.println("guardarInfoEstacoes");
        String nomeFicheiro = "coordinates.csv";
        ParisMetro instance = new ParisMetro();
        Graph<Estacao, Conexao> expResult = instance.distanceMap;
        Graph<Estacao, Conexao> result = instance.guardarInfoEstacoes(nomeFicheiro);
        assertEquals(expResult, result);

    }

    /**
     * Test of guardarInfoConexao method, of class ParisMetro.
     */
    @Test
    public void testGuardarInfoConexao() throws Exception {
        System.out.println("guardarInfoConexao");
        String nomeFicheiro = "connections.csv";
        ParisMetro instance = new ParisMetro();
        Graph<Estacao, Conexao> expResult = instance.distanceMap;
        Graph<Estacao, Conexao> result = instance.guardarInfoEstacoes("coordinates.csv");
        result = instance.guardarInfoConexao(nomeFicheiro);

        assertEquals(expResult.numEdges(), result.numEdges());
    }

    /**
     * Test of guardarInfoLinha method, of class ParisMetro.
     */
    @Test
    public void testGuardarInfoLinha() throws Exception {
        System.out.println("guardarInfoLinha");
        String nomeFicheiro = "lines_and_stations.csv";
        ParisMetro instance = new ParisMetro();
        ArrayList<Linha> expResult = instance.arrayLinha;
        ArrayList<Linha> result = instance.guardarInfoLinha(nomeFicheiro);
        assertEquals(expResult, result);
    }

    /**
     * Test of estacaoDesejada method, of class ParisMetro.
     */
    @Test
    public void testEstacaoDesejada() throws FileNotFoundException {
        System.out.println("estacaoDesejada");
        String nome = "George V";
        ParisMetro instance = new ParisMetro();
        Estacao expResult = new Estacao("George V", 2.30057, 48.87203);
        Estacao result = instance.estacaoDesejada(nome);
        assertEquals(expResult, result);
    }

    /**
     * Test of conexo method, of class ParisMetro.
     */
    @Test
    public void testConexo() throws FileNotFoundException {
        System.out.println("conexo");
        ParisMetro instance = new ParisMetro();
        int expResult = 299;
        LinkedList<LinkedList<Estacao>> result = instance.conexo();
        assertEquals(expResult, result.element().size());

    }

    /**
     * Test of createPercursoMinEstacoes method, of class ParisMetro.
     */
    @Test
    public void testCreatePercursoMinEstacoes() throws FileNotFoundException {
        System.out.println("createPercursoMinEstacoes");
        String einicio = "Charonne";
        String efinal = "Duroc";
        double instante = 0.0;

        ParisMetro instance = new ParisMetro();

        Estacao e1 = new Estacao("Charonne", 2.38464, 48.85513);
        Estacao e2 = new Estacao("Rue des Boulets", 2.38903, 48.85255);
        Estacao e3 = new Estacao("Nation", 2.39581, 48.84877);
        Estacao e4 = new Estacao("Reuilly Diderot", 2.38657, 48.84732);
        Estacao e5 = new Estacao("Gare de Lyon", 2.37376, 48.84468);
        Estacao e6 = new Estacao("Chatelet", 2.34722, 48.85877);
        Estacao e7 = new Estacao("Pyramides", 2.33392, 48.86648);
        Estacao e8 = new Estacao("Madeleine", 2.32446, 48.86978);
        Estacao e9 = new Estacao("Concorde", 2.32111, 48.86549);
        Estacao e10 = new Estacao("Invalides", 2.31329, 48.86297);
        Estacao e11 = new Estacao("Varenne", 2.31493, 48.85623);
        Estacao e12 = new Estacao("St Francois Xavier", 2.31427, 48.85167);
        Estacao e13 = new Estacao("Duroc", 2.31648, 48.8468);
        LinkedList<Estacao> list = new LinkedList<>();
        list.add(e1);
        list.add(e2);
        list.add(e3);
        list.add(e4);
        list.add(e5);
        list.add(e6);
        list.add(e7);
        list.add(e8);
        list.add(e9);
        list.add(e10);
        list.add(e11);
        list.add(e12);
        list.add(e13);
        LinkedList<Double> tempos = new LinkedList<>();
        tempos.add(0.0);
        tempos.add(1.0);
        tempos.add(2.0);
        tempos.add(3.0);
        tempos.add(5.0);
        tempos.add(8.0);
        tempos.add(10.0);
        tempos.add(12.0);
        tempos.add(13.0);
        tempos.add(15.0);
        tempos.add(17.0);
        tempos.add(18.0);
        tempos.add(19.0);

        Percurso expResult = new Percurso(list, tempos, instante);
        Percurso result = instance.createPercursoMinEstacoes(instance.estacaoDesejada(einicio), instance.estacaoDesejada(efinal), 0.0);
        assertEquals(expResult, result);
    }

    /**
     * Test of createPercursoMinTempo method, of class ParisMetro.
     */
    @Test
    public void testCreatePercursoMinTempo() throws FileNotFoundException {
        System.out.println("createPercursoMinTempo");

        String einicio = "Charonne";
        String efinal = "Duroc";
        Double instante = 0.0;
        ParisMetro instance = new ParisMetro();

        Estacao e1 = new Estacao("Charonne", 2.38464, 48.85513);
        Estacao e2 = new Estacao("Rue des Boulets", 2.38903, 48.85255);
        Estacao e3 = new Estacao("Nation", 2.39581, 48.84877);
        Estacao e4 = new Estacao("Reuilly Diderot", 2.38657, 48.84732);
        Estacao e5 = new Estacao("Gare de Lyon", 2.37376, 48.84468);
        Estacao e6 = new Estacao("Chatelet", 2.34722, 48.85877);
        Estacao e7 = new Estacao("Cite", 2.3462, 48.85578);
        Estacao e8 = new Estacao("Saint Michel", 2.34392, 48.85368);
        Estacao e9 = new Estacao("Odeon", 2.33882, 48.8524);
        Estacao e10 = new Estacao("Mabillon", 2.33543, 48.85297);
        Estacao e11 = new Estacao("Sevres Babylone", 2.32652, 48.85132);
        Estacao e12 = new Estacao("Vaneau", 2.32156, 48.84899);
        Estacao e13 = new Estacao("Duroc", 2.31648, 48.8468);

        LinkedList<Estacao> list = new LinkedList<>();
        list.add(e1);
        list.add(e2);
        list.add(e3);
        list.add(e4);
        list.add(e5);
        list.add(e6);
        list.add(e7);
        list.add(e8);
        list.add(e9);
        list.add(e10);
        list.add(e11);
        list.add(e12);
        list.add(e13);

        LinkedList<Double> tempos = new LinkedList<>();
        tempos.add(0.0);
        tempos.add(1.0);
        tempos.add(2.0);
        tempos.add(3.0);
        tempos.add(5.0);
        tempos.add(8.0);
        tempos.add(9.0);
        tempos.add(10.0);
        tempos.add(11.0);
        tempos.add(13.0);
        tempos.add(14.0);
        tempos.add(15.0);
        tempos.add(17.0);

        Percurso expResult = new Percurso(list, tempos, instante);
        Percurso result = instance.createPercursoMinTempo(instance.estacaoDesejada(einicio), instance.estacaoDesejada(efinal), 0.0);
        assertEquals(expResult, result);
    }
}
