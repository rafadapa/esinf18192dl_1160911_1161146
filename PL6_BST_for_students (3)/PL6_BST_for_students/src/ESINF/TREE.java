package ESINF;

import ESINF.BST;
import java.util.ArrayList;
import static java.util.Collections.copy;
import static java.util.Collections.list;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Stack;
import static javafx.scene.input.KeyCode.T;

/*
 * @author DEI-ESINF
 * @param <E>
 */
public class TREE<E extends Comparable<E>> extends BST<E>
{

    /*
   * @param element A valid element within the tree
   * @return true if the element exists in tree false otherwise
     */
    public boolean contains(E element)
    {
        if (element == null)
        {
            return false;
        }
        return (find(element, root) != null);
    }

    public boolean isLeaf(E element)
    {
        if (element == null)
        {
            return false;
        }
        Node<E> node = find(element, root);

        return ((node == null) ? false : (node.getLeft() == null && node.getRight() == null));

    }

    /*
   * build a list with all elements of the tree. The elements in the 
   * left subtree in ascending order and the elements in the right subtree 
   * in descending order. 
   *
   * @return    returns a list with the elements of the left subtree 
   * in ascending order and the elements in the right subtree is descending order.
     */
    public Iterable<E> ascdes()
    {
        List<E> list = new ArrayList<>();

        ascSubtree(root.getLeft(), list);

        list.add(root.getElement());

        desSubtree(root.getRight(), list);

        return list;
    }

    private void ascSubtree(Node<E> node, List<E> list)
    {
        if (node != null)
        {
            ascSubtree(node.getLeft(), list);

            list.add(node.getElement());

            ascSubtree(node.getRight(), list);
        }
    }

    private void desSubtree(Node<E> node, List<E> list)
    {
        if (node != null)
        {
            ascSubtree(node.getRight(), list);

            list.add(node.getElement());

            ascSubtree(node.getLeft(), list);
        }
    }

    /**
     * Returns the tree without leaves.
     *
     * @return tree without leaves
     */
    public BST<E> autumnTree()
    {
        BST<E> tree = new TREE<>();
        tree.root = copyrec(root);
        return tree;

    }

    private Node<E> copyrec(Node<E> node)
    {
        if (node == null)
        {
            return null;
        }
        if (node.getLeft() != null || node.getRight() != null)
        {
            return new Node(node.getElement(), copyrec(node.getLeft()), copyrec(node.getRight()));
        }
        return null;
    }

    /**
     * @return the the number of nodes by level.
     */
    public int[] numNodesByLevel()
    {
        int[] vect = new int[height() + 1];

        numNodesByLevel(root(), vect, 0);

        return vect;
    }

    private void numNodesByLevel(Node<E> node, int[] result, int level)
    {
        if (node == null)
        {
            return;
        }

        result[level] += 1;

        numNodesByLevel(node.getLeft(), result, level + 1);
        numNodesByLevel(node.getRight(), result, level + 1);

    }

}