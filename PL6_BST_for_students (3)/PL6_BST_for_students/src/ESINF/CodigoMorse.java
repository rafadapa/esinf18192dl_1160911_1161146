/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ESINF;

import Core.Morse;
import java.io.FileNotFoundException;
import java.util.Iterator;

/**
 *
 * @author Rafael Teixeira
 */
public class CodigoMorse {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
       
        MorseCodigo m = new MorseCodigo();
        
        m.lerFicheiros("morse.csv");
        
        BST<Morse> tree = m.tree;
        
//  
//        Iterator<Morse> iterator = tree.preOrder().iterator();
//
//        System.out.println("");
//
//        while (iterator.hasNext()) {
//
//            Morse n = iterator.next();
//
//         //System.out.println(n.getAlfaNumerica());
//            
//
//        }
  
        System.out.println(tree);
        
    
        String dois = m.descodificar("___ ._.. ._ / ___ / __ . .._ / _. ___ __ . / .._.. / ._. ._ .._. ._ . ._..");
        System.out.println(dois);
        
        BST<Morse> arvore= m.letra;
        System.out.println(arvore.toString());
   
     
    String quatro = m.codificacao("OLA");
        System.out.println(quatro);
        
        
      String p1 = "L";
      String p2 = "P";
      String result = m.seqInicial(p1, p2);
      if(result.equals("")){
          System.out.println("Não têm nenhuma sequência!");
      } else {
        System.out.println(result);
    }
    
   String auxiliar = m.codificacaoTreeToda("5OLA");
        System.out.println(auxiliar);
    }
    
}
