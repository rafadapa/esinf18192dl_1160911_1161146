/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ESINF;

import Core.Morse;
import PL.TextWord;
import java.io.File;
import java.io.FileNotFoundException;
import static java.lang.Math.E;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import static javafx.scene.input.KeyCode.E;

/**
 *
 * @author 1160911@isep.ipp.pt
 */
public class MorseCodigo extends BST<Morse> {

    BST<Morse> tree = new BST();
    ArrayList<String> l = new ArrayList<>();
    
    public MorseCodigo() throws FileNotFoundException {
       tree = guardarTree("morse.csv");
       letra = treeLetras();
    }
    
    

    //ALINEA 1
   
    BST<Morse> letra = new BST();

    public ArrayList<String> lerFicheiros(String nomeF) throws FileNotFoundException {

        ArrayList<String> ficheiroLido = new ArrayList<>();

        Scanner fich = new Scanner(new File(nomeF));

        while (fich.hasNext()) {

            ficheiroLido.add(fich.nextLine());

        }

        return ficheiroLido;

    }

    public BST guardarTree(String nomeFicheiro) throws FileNotFoundException {

        ArrayList<String> lista = this.lerFicheiros(nomeFicheiro);
        Morse n = new Morse("n", "START", "n");
        tree.insert(n);
        for (String l : lista) {
            String[] array = l.split(" ");
            Morse m = new Morse(array[0], array[1], array[2]);
            tree.insert(m);
        }
        return tree;
    }

    public String descodificar(String cod) throws FileNotFoundException {
        String aux;
        String palavra = "";
        String[] s = cod.split(" ");
        for (int i = 0; i < s.length; i++) {
            if (s[i].equals("/")) {
                palavra = palavra + " ";
            } else {
                Morse m = new Morse(s[i]);
                m = tree.find(m).getElement();
                aux = m.getAlfaNumerica();
                palavra = palavra + aux;
            }
        }
        return palavra;
    }

    public BST treeLetras() {
        Map<Integer, List<Morse>> map = new HashMap<>();
        List<Morse> aux = new ArrayList<>();
        String al;
        Morse n = new Morse("n", "START", "n");
        letra.insert(n);
        map = tree.nodesByLevel();
        for (int i = 0; i < map.size(); i++) {
            aux = map.get(i);
            for (Morse m : aux) {
                al = m.getClasseSimbolo();
                if (al.equals("Letter")) {
                    letra.insert(m);
                }
            }
        }
        return letra;
    }

    public String codificacao(String cod) {
        String[] s = cod.split("");
        String classe = "Letter";
        String palavra = "";
        Map<Integer, List<Morse>> map = new HashMap<>();
        List<Morse> aux = new ArrayList<>();
        map = letra.nodesByLevel();
        for (int i = 0; i < s.length; i++) {
            for (int j = 0; j < map.size(); j++) {
                Morse m = new Morse(s[i], classe);
                aux = map.get(j);
                for (Morse morse : aux) {
                    if (m.getAlfaNumerica().equals(morse.getAlfaNumerica())) {
                        palavra = palavra + morse.getMorse() + " ";
                    }
                }
            }
        }
        return palavra;
    }

    public String codificacaoTreeToda(String cod) {
        String classe = "Letter,Non-English,Number,Punctuation,Prosign";
        String[] s = cod.split("");
        String[] c = classe.split(",");
        String palavra = "";
        Map<Integer, List<Morse>> map = new HashMap<>();
        List<Morse> aux = new ArrayList<>();
        map = tree.nodesByLevel();
        for (int i = 0; i < s.length; i++) {
            for (int j = 0; j < map.size(); j++) {
                for (int k = 0; k < c.length; k++) {
                    Morse m = new Morse(s[i], c[k]);
                    aux = map.get(j);
                    for (Morse morse : aux) {
                        if (m.getAlfaNumerica().equals(morse.getAlfaNumerica()) && m.getClasseSimbolo().equals(morse.getClasseSimbolo())) {
                            palavra = palavra + morse.getMorse() + " ";
                        }
                    }
                }
            }
        }
        return palavra;
    }

    public String seqInicial(String s1, String s2) {
        String lowest = "";
        String cod1 = "";
        String cod2 = "";
        cod1 = codificacaoTreeToda(s1);
        cod2 = codificacaoTreeToda(s2);
        String[] sum = cod1.split("");
        String[] sdois = cod2.split("");
        if (sum.length < sdois.length) {
            for (int i = 0; i < sum.length; i++) {
                if (sum[i].equals(sdois[i])) {
                    lowest = lowest + sum[i];
                } else {
                    return lowest;
                }
            }
        } else {
            for (int i = 0; i < sdois.length; i++) {
                if (sdois[i].equals(sum[i])) {
                    lowest = lowest + sdois[i];
                } else {
                    return lowest;
                }
            }
        }

        return lowest;
    }

//    public List<List<String>> ocorrencias(ArrayList<String> arr) {
//        List<List<String>> ocorrencias = new ArrayList<>();
//        String aux = "";
//        String alfa = "";
//        List<String> letter = new ArrayList<>();
//        List<String> non = new ArrayList<>();
//        List<String> number = new ArrayList<>();
//        List<String> punctuation = new ArrayList<>();
//        ocorrencias.add(letter);
//        ocorrencias.add(non);
//        ocorrencias.add(number);
//        ocorrencias.add(punctuation);
//        for (int i = 0; i < arr.size(); i++) {
//            aux = arr.get(i);
//            String[] s = aux.split(" ");
//            alfa = descodificar(s[i]);
//            
//        }
//        return ocorrencias;
//    }

}
