/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

import java.util.Objects;

/**
 *
 * @author Rafael Teixeira <1160911@isep.ipp.pt>
 */
public class Morse implements Comparable<Morse> {

    private String morse;
    private String alfaNumerica;
    private String classeSimbolo;

    public Morse() {
    }

    public Morse(String morse) {
        this.morse = morse;
    }

    public Morse(String alfaNumerica, String classeSimbolo) {
        this.alfaNumerica = alfaNumerica;
        this.classeSimbolo = classeSimbolo;
    }
    
    

    public Morse(String morse, String alfaNumerica, String classeSimbolo) {
        this.morse = morse;
        this.alfaNumerica = alfaNumerica;
        this.classeSimbolo = classeSimbolo;
    }

    public String getMorse() {
        return morse;
    }

    public void setMorse(String morse) {
        this.morse = morse;
    }

    public String getAlfaNumerica() {
        return alfaNumerica;
    }

    public void setAlfaNumerica(String alfaNumerica) {
        this.alfaNumerica = alfaNumerica;
    }

    public String getClasseSimbolo() {
        return classeSimbolo;
    }

    public void setClasseSimbolo(String classeSimbolo) {
        this.classeSimbolo = classeSimbolo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.morse);
        hash = 23 * hash + Objects.hashCode(this.alfaNumerica);
        hash = 23 * hash + Objects.hashCode(this.classeSimbolo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Morse other = (Morse) obj;
        if (!Objects.equals(this.morse, other.morse)) {
            return false;
        }
        if (!Objects.equals(this.alfaNumerica, other.alfaNumerica)) {
            return false;
        }
        if (!Objects.equals(this.classeSimbolo, other.classeSimbolo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return alfaNumerica;
    }

    @Override
    public int compareTo(Morse m) {
        String aux = null;
        int i = 0;
        int j = 0;

        if (m == null) {
            return -1;
        }

        aux = ((Morse) m).getMorse();

        do {
            if (aux.charAt(i) == '.' && this.getMorse().charAt(i) != aux.charAt(i)) {
                j = 1;
            } else if (aux.charAt(i) == '_' && this.getMorse().charAt(i) != aux.charAt(i)) {
                j = -1;
            }

            i++;

        } while (j == 0 && i < this.getMorse().length() && i < aux.length());

        if (j != 0 || this.getMorse().length() == aux.length()) {
            return j;
        }

        if (aux.length() > this.getMorse().length()) {
            if (aux.charAt(i) == '.') {
                return 1;
            } else {
                return -1;
            }
        } else {
            if (this.getMorse().charAt(i) == '.') {
                return 1;
            } else {
                return -1;
            }
        }

    }

}
