/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ESINF;

import Core.Morse;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Toshiba
 */
public class MorseCodigoTest {
    
    public MorseCodigoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of lerFicheiros method, of class MorseCodigo.
     */
    @Test
    public void testLerFicheiros() throws Exception {
        System.out.println("lerFicheiros");
        String nomeF = "testLs.csv";
        MorseCodigo instance = new MorseCodigo();
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add(". E Letter");
        ArrayList<String> result = instance.lerFicheiros(nomeF);
        assertEquals(expResult, result);
    }

    /**
     * Test of guardarTree method, of class MorseCodigo.
     */
    @Test
    public void testGuardarTree() throws Exception {
        System.out.println("guardarTree");
        String nomeFicheiro = "morse.csv";
        MorseCodigo instance = new MorseCodigo();
        BST expResult = instance.tree;
        BST result = instance.guardarTree(nomeFicheiro);
        assertEquals(expResult, result);
    }

    /**
     * Test of descodificar method, of class MorseCodigo.
     */
    @Test
    public void testDescodificar() throws FileNotFoundException {
        System.out.println("descodificar");
        String cod = "___ ._.. ._ / ___ / __ . .._ / _. ___ __ . / .._.. / ._. ._ .._. ._ . ._..";
        MorseCodigo instance = new MorseCodigo();
        System.out.println(instance.tree);
        String expResult = "OLA O MEU NOME É RAFAEL";
        String result = instance.descodificar(cod);
        assertEquals(expResult, result);
    }

    /**
     * Test of treeLetras method, of class MorseCodigo.
     */
    @Test
    public void testTreeLetras() throws FileNotFoundException {
        System.out.println("treeLetras");
        MorseCodigo instance = new MorseCodigo();
        BST expResult = instance.letra;
        BST result = instance.treeLetras();
        assertEquals(expResult, result);
    }

    
    /**
     * Test of codificacao method, of class MorseCodigo.
     */
    @Test
    public void testCodificacao() throws FileNotFoundException {
        System.out.println("codificacao");
        String cod = "OLA";
        MorseCodigo instance = new MorseCodigo();
        String expResult = "___ ._.. ._ ";
        String result = instance.codificacao(cod);
        assertEquals(expResult, result);
    }

    /**
     * Test of codificacaoTreeToda method, of class MorseCodigo.
     */
    @Test
    public void testCodificacaoTreeToda() throws FileNotFoundException {
        System.out.println("codificacaoTreeToda");
        String cod = "LUIS";
        MorseCodigo instance = new MorseCodigo();
        String expResult = "._.. .._ .. ... ";
        String result = instance.codificacaoTreeToda(cod);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of seqInicial method, of class MorseCodigo.
     */
    @Test
    public void testSeqInicial() throws FileNotFoundException {
        System.out.println("seqInicial");
        String s1 = "F";
        String s2 = "H";
        MorseCodigo instance = new MorseCodigo();
        String expResult = "..";
        String result = instance.seqInicial(s1, s2);
        assertEquals(expResult, result);
        
    }
    
}
