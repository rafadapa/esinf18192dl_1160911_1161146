/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Run;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import model.Bilhete;
import model.Estacao;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1160911@isep.ipp.pt
 */
public class MetroProjetoTest {
    
    public MetroProjetoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of lerFicheiros method, of class MetroProjeto.
     */
    @Test
    public void testLerFicheiros() throws Exception {
        System.out.println("lerFicheiros");
        String nomeF = "teste.txt";
        MetroProjeto instance = new MetroProjeto();
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("2,E2,C3");
        ArrayList<String> result = instance.lerFicheiros(nomeF);
        assertEquals(expResult, result);
    }

    /**
     * Test of guardarInfoEstacoes method, of class MetroProjeto.
     */
    @Test
    public void testGuardarInfoEstacoes() throws Exception {
        System.out.println("guardarInfoEstacoes");
        String nomeFicheiro = "teste.txt";
        MetroProjeto instance = new MetroProjeto();
        DoublyLinkedList<Estacao> expResult = new DoublyLinkedList<>();
        Estacao e = new Estacao(2,"E2","C3", new HashSet<>());
        expResult.addLast(e);
        DoublyLinkedList<Estacao> result = instance.guardarInfoEstacoes(nomeFicheiro);
        assertEquals(expResult.first(), result.first());
    }

    /**
     * Test of guardarInfoBilhetes method, of class MetroProjeto.
     */
    @Test
    public void testGuardarInfoBilhetes() throws Exception {
        System.out.println("guardarInfoBilhetes");
        String nomeFicheiro = "teste2.txt";
        MetroProjeto instance = new MetroProjeto();
        HashSet<Bilhete> expResult = new HashSet<>();
        Estacao e = new Estacao(2,"E2","C3", new HashSet<>());
        instance.linha.addLast(e);
        Bilhete b = new Bilhete(1,"Z2",e,e);
        HashSet<Bilhete> result = instance.guardarInfoBilhetes(nomeFicheiro);
        assertEquals(expResult, result);
    }

    /**
     * Test of estacaoDesejada method, of class MetroProjeto.
     */
    @Test
    public void testEstacaoDesejada() {
        System.out.println("estacaoDesejada");
        int num = 2;
        MetroProjeto instance = new MetroProjeto();
        Estacao expResult = new Estacao(2,"E2","C3", new HashSet<>());
        instance.linha.addLast(expResult);
        Estacao result = instance.estacaoDesejada(num);
        assertEquals(expResult, result);
    }

    /**
     * Test of numeroUtilizadoresPorEstacao method, of class MetroProjeto.
     */
    @Test
    public void testNumeroUtilizadoresPorEstacao() {
        System.out.println("numeroUtilizadoresPorEstacao");
        MetroProjeto instance = new MetroProjeto();
        Map<Estacao, Integer> expResult = null;
        Map<Estacao, Integer> result = instance.numeroUtilizadoresPorEstacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of maiorSequenciaEstacoes method, of class MetroProjeto.
     */
    @Test
    public void testMaiorSequenciaEstacoes() {
        System.out.println("maiorSequenciaEstacoes");
        String tipoBilhete = "";
        MetroProjeto instance = new MetroProjeto();
        DoublyLinkedList<Estacao> expResult = null;
        DoublyLinkedList<Estacao> result = instance.maiorSequenciaEstacoes(tipoBilhete);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of obterBilhetesTransgressao method, of class MetroProjeto.
     */
    @Test
    public void testObterBilhetesTransgressao() {
        System.out.println("obterBilhetesTransgressao");
        MetroProjeto instance = new MetroProjeto();
        Set<Bilhete> expResult = null;
        Set<Bilhete> result = instance.obterBilhetesTransgressao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
