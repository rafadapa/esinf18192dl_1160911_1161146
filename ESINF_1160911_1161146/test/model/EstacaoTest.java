/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Toshiba
 */
public class EstacaoTest {
    
    public EstacaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNr_identificacao method, of class Estacao.
     */
    @Test
    public void testGetNr_identificacao() {
        System.out.println("getNr_identificacao");
        Estacao e = new Estacao(1,"12","c1",new HashSet<>());
        int expResult = 1;
        int result = e.getNr_identificacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNr_identificacao method, of class Estacao.
     */
    @Test
    public void testSetNr_identificacao() {
        System.out.println("setNr_identificacao");
        int nr_identificacao = 2;
        Estacao e = new Estacao(1,"12","c1",new HashSet<>());
        e.setNr_identificacao(nr_identificacao);
    }

    /**
     * Test of getDescricao method, of class Estacao.
     */
    @Test
    public void testGetDescricao() {
        System.out.println("getDescricao");
        Estacao e = new Estacao(1,"12","c1",new HashSet<>());
        String expResult = "12";
        String result = e.getDescricao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDescricao method, of class Estacao.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        String descricao = "124";
        Estacao e = new Estacao(1,"12","c1",new HashSet<>());
        e.setDescricao(descricao);
    }

    /**
     * Test of getZona method, of class Estacao.
     */
    @Test
    public void testGetZona() {
        System.out.println("getZona");
        Estacao e = new Estacao(1,"12","c1",new HashSet<>());
        String expResult = "c1";
        String result = e.getZona();
        assertEquals(expResult, result);
    }

    /**
     * Test of setZona method, of class Estacao.
     */
    @Test
    public void testSetZona() {
        System.out.println("setZona");
        String zona = "112";
        Estacao e = new Estacao(1,"12","c1",new HashSet<>());
        e.setZona(zona);
    }

    /**
     * Test of getListaBilhetes method, of class Estacao.
     */
    @Test
    public void testGetListaBilhetes() {
        System.out.println("getListaBilhetes");
        Estacao e = new Estacao(1,"12","c1",new HashSet<>());
        HashSet<Bilhete> expResult = new HashSet<>();
        HashSet<Bilhete> result = e.getListaBilhetes();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaBilhetes method, of class Estacao.
     */
    @Test
    public void testSetListaBilhetes() {
        System.out.println("setListaBilhetes");
        HashSet<Bilhete> listaBilhetes = new HashSet<>();
        Estacao e = new Estacao(1,"12","c1",new HashSet<>());
        e.setListaBilhetes(listaBilhetes);
    }

    /**
     * Test of hashCode method, of class Estacao.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Estacao instance = new Estacao(1,"12","c1",new HashSet<>());
        int expResult = instance.hashCode();
        int result = instance.hashCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class Estacao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Estacao e1 = new Estacao(1,"12","c1",new HashSet<>());
        boolean expResult = false;
        boolean result = e1.equals(obj);
        assertEquals(expResult, result);
    }

    
}
