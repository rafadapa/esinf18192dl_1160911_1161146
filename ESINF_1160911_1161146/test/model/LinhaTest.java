/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Run.DoublyLinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Toshiba
 */
public class LinhaTest {
    
    public LinhaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListEstacao method, of class Linha.
     */
    @Test
    public void testGetListEstacao() {
        System.out.println("getListEstacao");
        Linha instance = new Linha(new DoublyLinkedList<>());
        DoublyLinkedList<Estacao> expResult = new DoublyLinkedList<>();
        DoublyLinkedList<Estacao> result = instance.getListEstacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListEstacao method, of class Linha.
     */
    @Test
    public void testSetListEstacao() {
        System.out.println("setListEstacao");
        DoublyLinkedList<Estacao> listEstacao = new DoublyLinkedList<>();
        Linha instance = new Linha(new DoublyLinkedList<>());
        instance.setListEstacao(listEstacao);
    }

    /**
     * Test of hashCode method, of class Linha.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Linha instance = new Linha(new DoublyLinkedList<>());
        int expResult = instance.hashCode();
        int result = instance.hashCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class Linha.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Linha instance = new Linha(new DoublyLinkedList<>());
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);

    }
    
}
