/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Toshiba
 */
public class BilheteTest {
    
    public BilheteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNumero method, of class Bilhete.
     */
    @Test
    public void testGetNumero() {
        System.out.println("getNumero");
        Estacao e1 = new Estacao(1,"12","c1",new HashSet<>());
        Bilhete instance = new Bilhete(12,"Z3",e1, e1);
        int expResult = 12;
        int result = instance.getNumero();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNumero method, of class Bilhete.
     */
    @Test
    public void testSetNumero() {
        System.out.println("setNumero");
        int numero = 2;
        Estacao e1 = new Estacao(1,"12","c1",new HashSet<>());
        Bilhete instance = new Bilhete(12,"Z3",e1, e1);
        instance.setNumero(numero);
    }

    /**
     * Test of getTipo method, of class Bilhete.
     */
    @Test
    public void testGetTipo() {
        System.out.println("getTipo");
        Estacao e1 = new Estacao(1,"12","c1",new HashSet<>());
        Bilhete instance = new Bilhete(12,"Z3",e1, e1);
        String expResult = "Z3";
        String result = instance.getTipo();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTipo method, of class Bilhete.
     */
    @Test
    public void testSetTipo() {
        System.out.println("setTipo");
        String tipo = "2";
        Estacao e1 = new Estacao(1,"12","c1",new HashSet<>());
        Bilhete instance = new Bilhete(12,"Z3",e1, e1);
        instance.setTipo(tipo);
    }

    /**
     * Test of getValidarEntrada method, of class Bilhete.
     */
    @Test
    public void testGetValidarEntrada() {
        System.out.println("getValidarEntrada");
        Estacao e1 = new Estacao(1,"12","c1",new HashSet<>());
        Bilhete instance = new Bilhete(12,"Z3",e1, e1);
        Estacao expResult = e1;
        Estacao result = instance.getValidarEntrada();
        assertEquals(expResult, result);
    }

    /**
     * Test of setValidarEntrada method, of class Bilhete.
     */
    @Test
    public void testSetValidarEntrada() {
        System.out.println("setValidarEntrada");
        Estacao t = new Estacao(12,"12","c1",new HashSet<>());
        Estacao e1 = new Estacao(1,"12","c1",new HashSet<>());
        Bilhete instance = new Bilhete(12,"Z3",e1, e1);
        instance.setValidarEntrada(t);
    }

    /**
     * Test of getValidarSaida method, of class Bilhete.
     */
    @Test
    public void testGetValidarSaida() {
        System.out.println("getValidarSaida");
        Estacao e1 = new Estacao(1,"12","c1",new HashSet<>());
        Bilhete instance = new Bilhete(12,"Z3",e1, e1);
        Estacao expResult = e1;
        Estacao result = instance.getValidarSaida();
        assertEquals(expResult, result);
    }

    /**
     * Test of setValidarSaida method, of class Bilhete.
     */
    @Test
    public void testSetValidarSaida() {
        System.out.println("setValidarSaida");
        Estacao t = new Estacao(12,"12","c1",new HashSet<>());
        Estacao e1 = new Estacao(1,"12","c1",new HashSet<>());
        Bilhete instance = new Bilhete(12,"Z3",e1, e1);
        instance.setValidarSaida(t);
    }

    /**
     * Test of hashCode method, of class Bilhete.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Estacao e1 = new Estacao(1,"12","c1",new HashSet<>());
        Bilhete instance = new Bilhete(12,"Z3",e1, e1);
        int expResult = instance.hashCode();
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Bilhete.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Estacao e1 = new Estacao(1,"12","c1",new HashSet<>());
        Bilhete instance = new Bilhete(12,"Z3",e1, e1);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
