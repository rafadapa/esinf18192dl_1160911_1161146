/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Run;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import javafx.scene.Node;
import model.Bilhete;
import model.Bilhete;
import model.Estacao;
import model.Estacao;
import model.Linha;

/**
 *
 * @author 1160911@isep.ipp.pt / 1161146@isep.ipp.pt
 */
public class MetroProjeto {

    DoublyLinkedList<Estacao> linha;
    HashSet<Bilhete> listaBilhetes;

    MetroProjeto() {
        linha = new DoublyLinkedList<>();
        listaBilhetes = new HashSet<>();
    }

    public ArrayList<String> lerFicheiros(String nomeF) throws FileNotFoundException {

        ArrayList<String> ficheiroLido = new ArrayList<>();

        Scanner fich = new Scanner(new File(nomeF));

        while (fich.hasNext()) {

            ficheiroLido.add(fich.nextLine());

        }

        return ficheiroLido;

    }

    public DoublyLinkedList<Estacao> guardarInfoEstacoes(String nomeFicheiro) throws FileNotFoundException {

        ArrayList<String> lista = this.lerFicheiros(nomeFicheiro);

        for (String l : lista) {
            String[] array = l.split(",");
            Estacao e = new Estacao(Integer.parseInt(array[0]), array[1], array[2], new HashSet<Bilhete>());
            linha.addLast(e);
        }

        return linha;
    }

    public HashSet<Bilhete> guardarInfoBilhetes(String nomeFicheiro) throws FileNotFoundException {

        ArrayList<String> lista = this.lerFicheiros(nomeFicheiro);

        for (String l : lista) {
            String[] array = l.split(",");
            Bilhete b = new Bilhete(Integer.parseInt(array[0]), array[1], estacaoDesejada(Integer.parseInt(array[2])), estacaoDesejada(Integer.parseInt(array[3])));
            listaBilhetes.add(b);
        }

        return listaBilhetes;

    }

    public Estacao estacaoDesejada(int num) {

        Iterator<Estacao> iterador = this.linha.iterator();

        Estacao estacaoDesejada = null;

        while (iterador.hasNext()) {

            Estacao e = iterador.next();
            if (e.getNr_identificacao() == num) {
                estacaoDesejada = e;
                break;
            }

        }

        return estacaoDesejada;

    }
    
    public Map<Estacao, Integer> numeroUtilizadoresPorEstacao() {

        Map<Estacao, Integer> map = new HashMap<>();

        Iterator<Estacao> iter = this.linha.iterator();
        while (iter.hasNext()) {
            Estacao e = iter.next();
            int nr = e.getNr_identificacao();
            int c = 0;
            for (Bilhete b : listaBilhetes) {

                if (e.getNr_identificacao() == b.getValidarSaida().getNr_identificacao() || e.getNr_identificacao() == b.getValidarEntrada().getNr_identificacao() || (b.getValidarEntrada().getNr_identificacao() < e.getNr_identificacao() && b.getValidarSaida().getNr_identificacao() > e.getNr_identificacao()) || (b.getValidarEntrada().getNr_identificacao() > e.getNr_identificacao() && b.getValidarSaida().getNr_identificacao() < e.getNr_identificacao())) {
                    c++;
                }
            }
            map.put(e, c);
        }

        return map;
    }

    public DoublyLinkedList<Estacao> maiorSequenciaEstacoes(String tipoBilhete) {
        int cont = 0;
        ArrayList<String> zonasPassadas = new ArrayList<>();
        DoublyLinkedList<Estacao> temp = new DoublyLinkedList<>();
        int numZonas = 0;

        if (tipoBilhete.equals("Z2")) {
            numZonas = 2;
        } else if (tipoBilhete.equals("Z3")) {
            numZonas = 3;
        } else if (tipoBilhete.equals("Z4")) {
            numZonas = 4;
        } else if (tipoBilhete.equals("Z5")) {
            numZonas = 5;
        }

        for (Estacao estacao : linha) {
            while (zonasPassadas.size() <= (numZonas)) {
                if (!zonasPassadas.contains(estacao.getZona())) {
                    zonasPassadas.add(estacao.getZona());
                    temp.addLast(estacao);
                    
                    break;
                }
                temp.addLast(estacao);
                break;
            }
        }
        if (tipoBilhete != "Z5"){
        temp.removeLast();
        }
        return temp;

    }

public Set<Bilhete> obterBilhetesTransgressao() {

        List<String> zonas = new ArrayList<>();
        Set<Bilhete> resultado = new HashSet<>();
        int count = 0;

        for (Estacao estacao : linha) {
            if (!zonas.contains(estacao.getZona())) {
                zonas.add(estacao.getZona());
                count++;

                for (Bilhete bilhete : estacao.getListaBilhetes()) {

                    if (bilhete.getValidarSaida().getNr_identificacao()== estacao.getNr_identificacao() && Integer.parseInt(bilhete.getTipo().substring(1)) > count) {

                        resultado.add(bilhete);
                    }
                }
            }
        }

        return resultado;
    }

}
