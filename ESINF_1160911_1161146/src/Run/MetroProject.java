/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Run;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import model.Bilhete;
import model.Estacao;

/**
 *
 * @author 1160911@isep.ipp.pt
 */
public class MetroProject {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {

        MetroProjeto p = new MetroProjeto();

        p.lerFicheiros("fx_estacoes.txt");

        DoublyLinkedList<Estacao> list1 = p.guardarInfoEstacoes("fx_estacoes.txt");

        Iterator<Estacao> iterador = list1.iterator();

        while (iterador.hasNext()) {

            Estacao e = iterador.next();

            System.out.println(e.getNr_identificacao() + "," + e.getZona() + "," + e.getDescricao());

        }
        
        System.out.println("-------------------------------------------------------------------");
        

        HashSet<Bilhete> list2 = p.guardarInfoBilhetes("fx_viagens.txt");

        for (Bilhete b : list2) {

            System.out.println(b.getNumero() + "-" + b.getTipo() + "- ESTAÇÃO ENTRADA ->" + b.getValidarEntrada().getDescricao() + " ESTAÇÃO SAIDA ->" + b.getValidarSaida().getDescricao());
        }

        
        System.out.println("-------------------------------------------------------------------");
        
        Map<Estacao, Integer> map = p.numeroUtilizadoresPorEstacao();
        for (Estacao e : map.keySet()) {
            System.out.println("Nª da estacao: " + e.getNr_identificacao() + " - " + "Nº de utilizadores que passaram: " + map.get(e));
        }

        
            System.out.println("-------------------------------------------------------------------");

            DoublyLinkedList<Estacao> list3 = p.maiorSequenciaEstacoes("Z2");
            System.out.println("Z2 :");
            for (Estacao e : list3) {
                System.out.println(e.getNr_identificacao());
            }
        
            System.out.println("-------------------------------------------------------------------");
            
             DoublyLinkedList<Estacao> list4 = p.maiorSequenciaEstacoes("Z3");
             System.out.println("Z3 :");
             for (Estacao e : list4) {
                System.out.println(e.getNr_identificacao());
             }
             
             System.out.println("-------------------------------------------------------------------");
             
             DoublyLinkedList<Estacao> list5 = p.maiorSequenciaEstacoes("Z4");
             System.out.println("Z4 :");
             for (Estacao e : list5) {
                System.out.println(e.getNr_identificacao());
             }
             
             System.out.println("-------------------------------------------------------------------");
             
             DoublyLinkedList<Estacao> list6 = p.maiorSequenciaEstacoes("Z5");
             System.out.println("Z5 :");
             for (Estacao e : list6) {
                System.out.println(e.getNr_identificacao());
             }
             
             System.out.println("-------------------------------------------------------------------");
             
             
             
        System.out.println("Bilhetes Em Transgressão :");
        Set<Bilhete> list7 = p.obterBilhetesTransgressao();
        for (Bilhete b : list7) {
            System.out.println(b.getNumero());
        }

    }

}
