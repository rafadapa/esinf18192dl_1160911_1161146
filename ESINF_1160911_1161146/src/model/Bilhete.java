/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author 1160911@isep.ipp.pt
 */
public class Bilhete {
    
    private int numero;
    private String tipo;
    private Estacao validarEntrada;
    private Estacao validarSaida;

    public Bilhete(int numero, String tipo, Estacao validarEntrada, Estacao validarSaida) {
        this.numero = numero;
        this.tipo = tipo;
        this.validarEntrada = validarEntrada;
        this.validarSaida = validarSaida;
    }

    
    
    
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Estacao getValidarEntrada() {
        return validarEntrada;
    }

    public void setValidarEntrada(Estacao validarEntrada) {
        this.validarEntrada = validarEntrada;
    }

    public Estacao getValidarSaida() {
        return validarSaida;
    }

    public void setValidarSaida(Estacao validarSaida) {
        this.validarSaida = validarSaida;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.numero;
        hash = 47 * hash + Objects.hashCode(this.tipo);
        hash = 47 * hash + Objects.hashCode(this.validarEntrada);
        hash = 47 * hash + Objects.hashCode(this.validarSaida);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bilhete other = (Bilhete) obj;
        if (this.numero != other.numero) {
            return false;
        }
        if (!Objects.equals(this.tipo, other.tipo)) {
            return false;
        }
        if (!Objects.equals(this.validarEntrada, other.validarEntrada)) {
            return false;
        }
        if (!Objects.equals(this.validarSaida, other.validarSaida)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
