package model;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 1160911@isep.ipp.pt
 */
public class Estacao implements Comparator<Estacao> {
    
    
    private int nr_identificacao;
    private String descricao;
    private String zona;
    private HashSet<Bilhete> listaBilhetes;
  
    
    
    
    public Estacao (int nr_identificacao, String descricao, String zona, HashSet<Bilhete> listaBilheteEntrada) {
        this.nr_identificacao = nr_identificacao;
        this.descricao = descricao;
        this.zona = zona;
        this.listaBilhetes = listaBilheteEntrada;
 
        
    }

    public int getNr_identificacao() {
        return nr_identificacao;
    }

    public void setNr_identificacao(int nr_identificacao) {
        this.nr_identificacao = nr_identificacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public HashSet<Bilhete> getListaBilhetes() {
        return listaBilhetes;
    }

    public void setListaBilhetes(HashSet<Bilhete> listaBilhetes) {
        this.listaBilhetes = listaBilhetes;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.nr_identificacao;
        hash = 37 * hash + Objects.hashCode(this.descricao);
        hash = 37 * hash + Objects.hashCode(this.zona);
        hash = 37 * hash + Objects.hashCode(this.listaBilhetes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estacao other = (Estacao) obj;
        if (this.nr_identificacao != other.nr_identificacao) {
            return false;
        }
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        if (!Objects.equals(this.zona, other.zona)) {
            return false;
        }
        if (!Objects.equals(this.listaBilhetes, other.listaBilhetes)) {
            return false;
        }
        return true;
    }


    @Override
    public int compare(Estacao o1, Estacao o2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }





    
    
    
    
    
    
}
