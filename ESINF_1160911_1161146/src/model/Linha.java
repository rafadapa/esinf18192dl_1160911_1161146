/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import Run.DoublyLinkedList;
import java.util.Objects;

/**
 *
 * @author 1160911@isep.ipp.pt
 */
public class Linha {

    private DoublyLinkedList<Estacao> listEstacao;

    public Linha(DoublyLinkedList<Estacao> listEstacao) {
        this.listEstacao = listEstacao;
    }

    public DoublyLinkedList<Estacao> getListEstacao() {
        return listEstacao;
    }

    public void setListEstacao(DoublyLinkedList<Estacao> listEstacao) {
        this.listEstacao = listEstacao;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.listEstacao);
        return hash;
    }
    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Linha other = (Linha) obj;
        if (!Objects.equals(this.listEstacao, other.listEstacao)) {
            return false;
        }
        return true;
    }
    
    

}
